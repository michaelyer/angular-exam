import { SavedPostsComponent } from './saved-posts/saved-posts.component';
import { CommentsComponent } from './comments/comments.component';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';
import { SecureInnerPagesGuard } from './guards/secure-inner-pages.guard';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import { SuccessComponent } from './success/success.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { RouterModule,Routes} from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from './guards/auth.guard';


const appRoutes:Routes = [
    {path: '', redirectTo: '/login', pathMatch:'full'},
    {path: 'login', component: LoginComponent },
    {path: 'signup', component: SignupComponent},
    {path: 'verify-email-address', component: VerifyEmailComponent},
    {path: 'success', component: SuccessComponent},
    {path: 'classify', component: ClassifiedComponent, canActivate: [AuthGuard]},
    {path: 'docform', component: DocformComponent, canActivate: [AuthGuard]},
    {path: 'posts', component: BlogPostsComponent, canActivate: [AuthGuard]},
    {path: 'comments/:postId', component: CommentsComponent, canActivate: [AuthGuard] },
    {path: 'savedPosts', component: SavedPostsComponent, canActivate: [AuthGuard]},

];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]  
})
export class RoutingModule{

}