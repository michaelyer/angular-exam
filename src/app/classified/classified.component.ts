import { User } from 'firebase';
import { DBuser } from './../Dbuser';
import { AuthService } from './../auth.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ImageService } from '../image.service';
import { Observable } from 'rxjs';



export interface Classify {
  value:string;
  viewValue:string;
}

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {


  classifyCategory: Classify[] = [
    {value: 'business-0', viewValue: 'business'},
    {value: 'entertainment-1', viewValue: 'entertainment'},
    {value: 'politics-2', viewValue: 'politics'},
    {value: 'sport-3', viewValue: 'sport'},
    {value: 'tech-4', viewValue: 'tech'}
  ]

  classifyResult:string;
  user: Observable <User>;


  addClassification(){
    
  }


  category:string = "Loading...";
  categoryImage:string; 
  constructor(public classifyService:ClassifyService,
              public imageService:ImageService,public auth:AuthService) {}

  ngOnInit() {
    console.log(this.auth.isLoggedIn)
    console.log("user data: ", this.auth.getUserId())
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )
  }
}
