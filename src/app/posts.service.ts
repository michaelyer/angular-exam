import { DBuser } from './Dbuser';
import { map } from 'rxjs/operators';
import { Comment } from './comment';
import { Post } from './post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';






@Injectable({
  providedIn: 'root'
})
export class PostsService {

 
  

   POSTURL  = "https://jsonplaceholder.typicode.com/posts";

   commentUrl = "https://jsonplaceholder.typicode.com/comments";

   userUrl = "https://jsonplaceholder.typicode.com/users"
  

  // private posts:Observable<Post[]>;
  // private comments:Observable<Comment[]>

  constructor(private http:HttpClient,  private db:AngularFirestore) { }

  postCollection:AngularFirestoreCollection; 

  userCollection:AngularFirestoreCollection = this.db.collection('users');

  


  getAllComments(){
   return this.http.get<Comment[]>(`${this.commentUrl}`);
    
  }

  getPostsData(): Observable<Post[]>{
    const res = this.http.get<Post[]>(`${this.POSTURL}`)
    return res;
}

  getPostsId(id:number):Observable<Post[]>{
    const res = this.http.get<Post[]>(`${this.POSTURL}.{id}`)
    return res;
  }

  getCollections(userId):Observable<any[]>{
    this.postCollection = this.db.collection(`users/${userId}/posts`);
    return this.postCollection.snapshotChanges().pipe(
      map(res => res.map(
            r=> {
              const data = r.payload.doc.data();
              data.id = r.payload.doc.data  ;
              return data ; 
            }
      ))
    )
  }

  saveToFirebase(userId:string, post:Post){
    const postSave = {id:post.id,title:post.title,body:post.body,likes:0}
    this.userCollection.doc(userId).collection('posts').add(postSave);
  }



  addLikes(userId:string, likes:number, id:string){
    this.db.doc(`users/${userId}/posts/${id}`).update({
      likes:likes+1,
    });
  }
}
  

