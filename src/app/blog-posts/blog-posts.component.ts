import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { post } from 'selenium-webdriver/http';
import { Comment } from './../comment';
import { PostsService } from './../posts.service';
import { Post } from './../post';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit {

  panelOpenState = false;

  post$:Observable<Post>;
  posts$:Observable<Post[]>

  userId:string;

  // comments$:Observable <Comment[];
 
  save(post:Post){
    this.postsService.saveToFirebase(this.userId, post);
    this.router.navigate(['/savedPosts']);
  }

  addLikes(id, likes){
    this.postsService.addLikes(this.userId, likes, id); 
  }

  constructor(private postsService:PostsService, private authService:AuthService, private router:Router) { }

  ngOnInit() {

    this.posts$ = this.postsService.getPostsData();
     
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid; 
        this.posts$ = this.postsService.getCollections(this.userId)})
      }


    

    // this.comments$ = this.postsService.getAllComments(); 
 
  }









