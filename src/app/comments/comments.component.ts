import { User } from 'firebase';
import { PostsService } from './../posts.service';
import { Post } from './../post';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Comment } from './../comment'


@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  comments$:Comment[];
  
  postId; 

  postName:string;
  
  Blogpost$:Observable<Post[]>;

 



  constructor(private router:Router, private postService: PostsService, private route: ActivatedRoute ) { }

  ngOnInit() {

  this.postService.getAllComments().subscribe( res => this.comments$ = res);
  this.postId = this.route.snapshot.paramMap.get('postid');
    

}
}
