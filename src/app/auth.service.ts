import { DBuser } from './Dbuser';
import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore , AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;
  userId: string;
  userEmail: string;

  user$: Observable<DBuser>;

  postDb:AngularFirestoreCollection; 

  testUser:Observable<DBuser> ; 

  

  constructor(public afs: AngularFirestore, public afAuth: AngularFireAuth,
    public router: Router, public ngZone: NgZone, private db:AngularFirestore) {
      this.afAuth.authState.subscribe(
        user => {
          if (user){
            this.userData = user;
            localStorage.setItem('user', JSON.stringify(this.userData));
            JSON.parse(localStorage.getItem('user'));
          }
          else{
            localStorage.setItem('user', null);
            JSON.parse(localStorage.getItem('user'));
          }
        }
      )
     }

  SetUserData(user){
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: DBuser = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified
    }
    this.userId = user.uid;
    return userRef.set(userData, {
      merge: true
    })
  }

  LogIn(email, password){
    
    return this.afAuth.auth.signInWithEmailAndPassword(email, password).
    then((result) => {
      this.ngZone.run(() => {
        this.router.navigate(['success']);
        this.router.navigate(['/success']);
      });
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error.message)
    });
  }

  SignUp(email, password){
    return this.afAuth.auth.createUserWithEmailAndPassword(email,password).
    then((result) =>{
      this.SendVerificationMail();
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error.message);
    })
  }

  SendVerificationMail(){
    return this.afAuth.auth.currentUser.sendEmailVerification().
    then(() => {
      this.router.navigate(['/verify-email-address']);
    })
  }

  ForgetPassword(passwordResetEmail){
    return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
      window.alert('Password reset email sent, check your inbox.');
    }).catch((error) => {
      window.alert(error)
    })
  }

  get isLoggedIn(): boolean{
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false) ? true : false;
  }

  SignOut(){
    return this.afAuth.auth.signOut().then(() =>{
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    })
  }

  getUserId(){
    return this.userId;
  }

  getUser() {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    return user;
  }

  

  getDbData(userId): Observable<any[]>{
    this.postDb = this.db.collection(`users/${userId}/posts`);
    console.log('DB created ');
    return this.postDb.snapshotChanges().pipe(
      map(res => res.map(r => {
        const data = r.payload.doc.data();
        data.id = r.payload.doc.id;
        return { ...data };
      }))
    );
  }
  }

  

