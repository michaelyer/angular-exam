import { DBuser } from './../Dbuser';
import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {

  userId:string;
  userEmail:String;
  user$: Observable<DBuser>;

  constructor(private auth:AuthService) { }

  ngOnInit() {
    this.userEmail = this.auth.getUser().email;

    // this.userEmail = this.authService.getUser().email;
    // this.authService.user.subscribe(
    //   user => {
    //     this.userId = user.uid;
    //     this.userEmail = user.email;})

}
}


