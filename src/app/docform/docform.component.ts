import { Router } from '@angular/router';
import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  url:string;
  text:string;
  onSubmit(){
    this.classifyService.doc = this.text;
    this.router.navigate(['/classify']);
  }
  constructor(private classifyService:ClassifyService, 
              public imageService:ImageService,
              private router:Router ) { }

  ngOnInit() {  
  }

}
